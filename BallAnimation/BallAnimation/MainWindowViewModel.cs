﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows;
using System.Windows.Shapes;
using System.Threading;
using System.Windows.Data;

namespace BallAnimation
{
    class MainWindowViewModel : INotifyPropertyChanged
    {
        private const int xMax = 1360;
        private const int yMax = 768;

        private ImageBrush _imageSoursce;
        private ObservableCollection<Rectangle> _balls;
        private List<BallViewModel> _ballContexts;
        private MainWindow _window;

        public MainWindowViewModel()
        {
            _ballContexts = new List<BallViewModel>();
            _imageSoursce = new ImageBrush(new BitmapImage(new Uri("ball.png", UriKind.Relative)));
            _balls = new ObservableCollection<Rectangle>();
            _window = new MainWindow() { DataContext = this };
            _window.Show();

            Canvas d = new Canvas();

            var context = new BallViewModel(100, 100, 205);
            var ball = CreateBall(context);
            _balls.Add(ball);
            _ballContexts.Add(context);
            context.ChangeDirection(1, 3);

            Task.Factory.StartNew(() => StartAnimation());
        }

        public ObservableCollection<Rectangle> Balls
        {
            get
            {
                return _balls;
            }
        }

        private void StartAnimation()
        {


            while (true)
            {
                for (var i = 0; i < _balls.Count; i++)
                {
                    var ball = _ballContexts[i];
                    ball.Move();


                    _window.Dispatcher.Invoke((Action)(() =>
                    {
                        Canvas.SetTop(_balls[i], ball.Top);
                        Canvas.SetLeft(_balls[i], ball.Left);
                    }));

                    if ((ball.Left >= xMax - ball.Width || ball.Left <= 0 || ball.Top >= yMax - ball.Width || ball.Top <= 0) && ball.MoveCount > 100)
                    {
                        _window.Dispatcher.Invoke((Action)(() =>
                        {
                            var context = new BallViewModel(ball.Top, ball.Left, ball.Width);
                            context.ChangeDirection(ball.TopStep, ball.LeftStep);
                            var newBall = CreateBall(context);

                            _balls.Add(newBall);
                            _ballContexts.Add(context);
                            ChangeDirection(ball);
                            ChangeDirection2(context);
                        }));
                    }
                }
                Thread.Sleep(10);
            }
        }

        private void ChangeDirection(BallViewModel ball)
        {
            Random random = new Random();
            double top = ball.TopStep;
            double left = ball.LeftStep;
            if (ball.Left <= 0 || ball.Left >= xMax - ball.Width)
            {

                left *= -1;
            }
            else
            {
                top *= -1;
            }
            ball.ChangeDirection(top, left);
        }

        private void ChangeDirection2(BallViewModel ball)
        {
            double top = ball.TopStep;
            double left = ball.LeftStep;

            top = ball.TopStep * -1;

            left = ball.LeftStep * -1;

            ball.ChangeDirection(top, left);
        }

        private Rectangle CreateBall(BallViewModel context)
        {
            Rectangle ball = new Rectangle() { Fill = _imageSoursce };

            //Binding widthBinding = new Binding("Width") { UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged, Mode = BindingMode.TwoWay };
            //Binding heightBinding = new Binding("Width");
            //Binding topBinding = new Binding("Top");
            //Binding leftBinding = new Binding("Left");
            //ball.SetBinding(Rectangle.WidthProperty, widthBinding);
            //ball.SetBinding(Rectangle.HeightProperty, heightBinding);
            //ball.SetBinding(Canvas.TopProperty, topBinding);
            //ball.SetBinding(Canvas.LeftProperty, leftBinding);
            ball.DataContext = context;
            return ball;
        }

        public void RaisePropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
    }
}
