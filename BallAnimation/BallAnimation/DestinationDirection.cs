﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace BallAnimation
{
    class BallViewModel
    {
        private int _move;

        private double _top;
        private double _left;
        private double _width;

        private double _leftStep;
        private double _topStep;

        public BallViewModel(double top, double left, double width)
        {
            Top = top;
            Left = left;
            Width = width;
        }

        public double LeftStep
        {
            get
            {
                return _leftStep;
            }
        }
        public double TopStep
        {
            get
            {
                return _topStep;
            }
        }


        public double Width
        {
            get
            {
                return _width;
            }
            set
            {
                _width = value;
                RaisePropertyChanged("Width");
            }
        }
        public double Top
        {
            get
            {
                return _top;
            }
            set
            {
                _top = value;
                RaisePropertyChanged("Top");
            }
        }
        public double Left
        {
            get
            {
                return _left;
            }
            set
            {
                _left = value;
                RaisePropertyChanged("Left");
            }
        }

        public int MoveCount
        {
            get
            {
                return _move;
            }
        }


        public void RaisePropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;

        public void ChangeDirection(double top, double left)
        {
            _move = 0;
            _leftStep = left;
            _topStep = top;
            Width -= 5;
        }


        public void Move()
        {
            _move++;
            Left += _leftStep;
            Top += _topStep;

        }
    }
}
