﻿using SharpGL;
using SharpGL.Enumerations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Sinus
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        public MainWindow()
        {
            InitializeComponent();
        }

        //створення графічних об'єктів
        private void OpenGLControl_OpenGLDraw(object sender, SharpGL.SceneGraph.OpenGLEventArgs args)
        {
            OpenGL gl = args.OpenGL;

            gl.Clear(OpenGL.GL_COLOR_BUFFER_BIT | OpenGL.GL_DEPTH_BUFFER_BIT);

            gl.LoadIdentity();
            if((bool)checkBox.IsChecked)
                DrawAxis(gl);
            if ((bool)checkBox1_Copy.IsChecked)
                DrawCurve(gl);
            if ((bool)checkBox1.IsChecked)
                DrawText(gl);

            gl.Flush();//для прискорення виконання функцій OpenGL
        }

        private void DrawAxis(OpenGL gl)
        {
            gl.Begin(OpenGL.GL_LINES);
            gl.Color(1.0f, 1.0f, 1.0f);

            //осі
            gl.Vertex(0, 1);
            gl.Vertex(0, -1);

            gl.Vertex(-1, 0);
            gl.Vertex(1, 0);

            //шкала х
            for(float i =- 0.75f; i < 0.99; i += 0.25f)
            {
                gl.Vertex(i, 0.03);
                gl.Vertex(i, -0.03);
            }
            gl.Vertex(-1, 0.05);
            gl.Vertex(-1, -0.05);
            //шкала у
            for (float i = -0.75f; i < 0.99; i += 0.25f)
            {
                gl.Vertex( 0.01, i);
                gl.Vertex( -0.01, i);
            }
            gl.Vertex(0.04, -1);
            gl.Vertex(-0.04, -1);

            gl.End();

            gl.Begin(OpenGL.GL_TRIANGLES);
            //стрілка для у
            gl.Vertex(0, 1);
            gl.Vertex(0.04, 0.96);
            gl.Vertex(-0.04, 0.96);

            //стрілка для х
            gl.Vertex(1, 0);
            gl.Vertex(0.95, 0.05);
            gl.Vertex(0.95, -0.05);
            gl.End();
        }

        private void DrawCurve(OpenGL gl)
        {
            gl.Begin(OpenGL.GL_POINTS);
            gl.Color(1.0f, 0.0f, 0.0f);

            for (double i = -Math.PI * 2; i <= Math.PI * 2; i += 0.001f)
            {
                gl.Color(Math.Sin(i), Math.Cos(i), 1 - Math.Sin(i));
                gl.Vertex(i/Math.PI/2, Math.Sin(i));
            }
            gl.End();
        }

        private void DrawText(OpenGL gl)
        {
            string[] x = new string[] { "-3pi/2", "-pi", "-pi/2", "0", "pi/2", "pi", "3pi/2" };
            string[] y = new string[] { "-0.75", "-0.5", "-0.25", "0", "0.25", "0.5", "0.75" };

            int j = 0;
            for (float i = 0.22f; i < 1.96; i += 0.25f, j++)
            {
                gl.DrawText((int)(i * Width / 2), (int)(Height / 2 - 55), 1, 1, 1, "arial", 14, x[j]);
            }

            j = 0;
            for (float i = 0.25f; i < 1.99; i += 0.25f, j++)
            {
                gl.DrawText((int)(Width / 2 + 5), (int)(i * Height / 2 - 10), 1, 1, 1, "arial", 14, x[j]);
            }
            gl.DrawText((int)(Width / 2 + 5), (int)(Height - 90), 1, 1, 1, "arial", 14, "Y");
            gl.DrawText((int)(Width - 40), (int)(Height / 2 - 55), 1, 1, 1, "arial", 14, "X");


        }

        private void OpenGLControl_OpenGLInitialized(object sender, SharpGL.SceneGraph.OpenGLEventArgs args)
        {
            args.OpenGL.Ortho(0, Width, 0, Height, -1.0, 1.0);
        }

        private void OpenGLControl_Resized(object sender, SharpGL.SceneGraph.OpenGLEventArgs args)
        {

        }
    }
}
